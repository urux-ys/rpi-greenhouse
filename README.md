# Raspbian
  install requierment using pip :
   ```
    [~]# sudo apt update && sudo apt install python-pip

    [~]# git clone https://github.com/Urux-ys/projet_serre.git

    [~]# cd projet_serre/

    [~/projet_serre]# sudo pip install -r requirements.txt

    [~/projet_serre]# sudo python main.py
   ```
 
# Hardware 

 use Adafruit I2C ADC module
 and use :
 *         A0 analog entry for light sensor
 *         A1 analog entry for heat sensor
 *         A3 analog entry for humidity sensor
 use pin number 
 *                11 for water control 
 *                13 for light control 
 *                15 for heat control  


# License 
This project is under the terms of the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

rpi-greenhouse  Copyright (C) 2018 Urux-ys

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses
    
